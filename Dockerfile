FROM php:7.1-cli

LABEL maintainer="j.roczniewski@trojmiasto.pl"

RUN set -ex; \
	\
	savedAptMark="$(apt-mark showmanual)"; \
	\
	apt-get update; \
	apt-get install -y --no-install-recommends \
		libfreetype6-dev \
		libgeoip-dev \
		libjpeg-dev \
		libldap2-dev \
		libpng-dev \
	; \
  apt-get update; \
  apt-get install -y cron; \
	debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)"; \
	docker-php-ext-configure gd --with-freetype-dir=/usr --with-png-dir=/usr --with-jpeg-dir=/usr; \
	docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch"; \
	docker-php-ext-install \
		gd \
		ldap \
		mysqli \
		opcache \
		pdo_mysql \
		zip; \
  \
	pecl install geoip-1.1.1; \
	pecl install redis-3.1.6; \
	\
	docker-php-ext-enable \
		geoip \
		redis ;

RUN touch /var/log/cron.log
COPY piwik-archive /usr/local/bin/piwik-archive
RUN chmod +x /usr/local/bin/piwik-archive
COPY crontab /etc/cron.d/piwik-archive
RUN chmod 0644 /etc/cron.d/piwik-archive
COPY php.ini /usr/local/etc/php/conf.d/php-piwik.ini

ADD start.sh /start.sh
RUN chmod 777 /start.sh
ENTRYPOINT ["/start.sh"]
